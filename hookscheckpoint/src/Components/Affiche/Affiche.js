import Carousel from 'react-bootstrap/Carousel';

function Affiche() {
  return (
    <Carousel data-bs-theme="dark">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/spiderman.jpeg"
          alt="First slide"
          style={{ width: '400px', height: '400px' }}
        />
        <Carousel.Caption style={{ color: 'red', fontWeight: 'bold' }}>
          <h5>First slide label</h5>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/Fastandfurious.jpeg"
          alt="Second slide"
          style={{ width: '400px', height: '400px' }}
        />
        <Carousel.Caption style={{ color: 'red', fontWeight: 'bold' }}>
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/naruto.jpg"
          alt="Third slide"
          style={{ width: '400px', height: '400px' }}
        />
        <Carousel.Caption style={{ color: 'red', fontWeight: 'bold' }}>
          <h5>Third slide label</h5>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Affiche;