// import logo from './logo.svg';
import Barre from "./Components/Barre/Barre";
import Affiche from './Components/Affiche/Affiche';
import FilmCard from './Components/Filmcard';


function App() {
  return (
    <div className="App">
      <Barre></Barre>
      <Affiche></Affiche>
      <FilmCard></FilmCard>
    </div>
  );
}

export default App;
